# scc-docker

Docker image to use [scc](https://github.com/boyter/scc).

## Usage

The latest Docker image built in CI can be usable with a tag
`registry.gitlab.com/musicscience37projects/docker/scc-docker:latest`
