#!/bin/bash

set -eux

cd $(dirname $0)

mkdir -p outputs
scc --format-multi "tabular:stdout,html:outputs/numerical-collection-cpp.html,openmetrics:outputs/numerical-collection-cpp.txt" samples/numerical-collection-cpp --exclude-dir vcpkg
scc --format-multi "tabular:stdout,html:outputs/numerical-collection-cpp-files.html" --by-file -s complexity samples/numerical-collection-cpp/include samples/numerical-collection-cpp/src
